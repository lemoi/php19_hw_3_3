<?php

namespace Core\Goods;

class Car extends Product // Машина
{
    private $color = 'Белый';
    private $speed = 0;

    public function __construct($name, $price)
    {
        parent::__construct($name, $price);
        $this->setType('машина');
    }

    public function drive($speed)
    {
        $this->speed = $speed;
    }

    public function stop()
    {
        $this->speed = 0;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
        return $this;
    }

    function getCost()
    {
        return $this->price;
    }
}
