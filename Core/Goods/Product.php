<?php

namespace Core\Goods;

abstract class Product // Товар
{
    protected $type;
    protected $maker;
    protected $name;
    protected $price;

    abstract function getCost();

    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    public function show() {
        echo "{$this->name} ({$this->type})";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getMaker()
    {
        return $this->maker;
    }

    public function setMaker(string $maker)
    {
        $this->maker = $maker;
        return $this;
    }
}
