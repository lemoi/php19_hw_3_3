<?php

namespace Core\Goods;

class Pen extends Product // Ручка
{
    private $color;
    private $automatic;

    public function __construct(string $name, float $price, bool $automatic = false, string $color = 'Синий')
    {
        parent::__construct($name, $price);
        $this->setType('ручка');
        $this->automatic = (bool)$automatic;
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function replaceRod(string $color) {
        $this->color = $color;
        return $this;
    }

    function getCost()
    {
        return $this->price;
    }
}
