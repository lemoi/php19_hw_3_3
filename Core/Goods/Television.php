<?php

namespace Core\Goods;

class Television extends Product // Телевизор
{
    private $diagonal;
    private $work = false;

    public function __construct($name, $price)
    {
        parent::__construct($name, $price);
        $this->setType('телевизор');
    }

    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
        return $this;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function on() {
        $this->work = true;
    }

    public function off() {
        $this->work = false;
    }

    function getCost()
    {
        return $this->price;
    }
}
