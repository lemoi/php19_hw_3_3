<?php

namespace Core\Basket;

use Core\Goods\Product;

class Basket
{
    private $Products = [];

    private function find(Product $product) {
        foreach ($this->Products as $key => $value) {
            if ($value['product'] == $product) {
                return $key;
            }
        }
        return false;
    }

    public function add(Product $product, int $number = 1) {
        if ($product->getCost() == 0) {
            throw new \Exception('Товар с нулевой стоимостью нельзя добавлять в корзину');
        };

        $key = $this->find($product);
        if ($key === false) {
            $this->Products[] = ['product' => $product, 'number' => $number];
        } else {
            $this->Products[$key]['number'] += $number;
        }
        return $this;
    }

    public function remove(Product $product, int $number = null) {
        $key = $this->find($product);
        if ($key !== false) {
            $numberBasket = $this->Products[$key]['number'];
            if (isset($number) && $number < $numberBasket)  {
                $this->Products[$key]['number'] -=  $number;
            } else {
                unset($this->Products[$key]);
            }
        }
        return $this;
    }

    public function getCost() {
        $cost = 0;
        foreach ($this->Products as $value) {
            $cost += $value['product']->getCost() * $value['number'];
        }
        return $cost;
    }

    public function show() {
        echo '<ul>';
        foreach ($this->Products as $value) {
            echo '<li>';
            $value['product']->show();
            echo ' ' . $value['number'] . ' по цене ' . $value['product']->getCost() . ' (за штуку)</li>';
        }
        echo '</ul>';
    }
}