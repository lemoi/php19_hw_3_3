<?php

namespace Core\Order;

use Core\Basket\Basket;

class Order
{
    protected $basket;
    protected $buyer;
    protected $number;

    public function __construct(Basket $basket, string $buyer, int $number)
    {
        $this->basket = $basket;
        $this->buyer = $buyer;
        $this->number = $number;
    }

    public function show() {
        echo "Заказ №{$this->number} на общую сумму {$this->basket->getCost()}:<br>";
        $this->basket->show();
    }
}